
export default [
  {
    path: '/',
    component: () => import('layouts/default'),
    children: [
      {
        path: '',
        name:'about',
        component: () => import('pages/main/About'),
        meta:{
          requiresAuth:true
        }
      },
      {
        path: '/liste',
        name:'liste',
        component: () => import('pages/main/Liste'),
        meta:{
          requiresAuth:true
        }
      },
      {
        path: '/kalender',
        name:'kalender',
        component: () => import('pages/main/Kalender'),
        meta:{
          requiresAuth:true
        }
      }
    ]
  },
  {
    path: '/termin',
    name: 'termin',
    component: () => import('pages/Termin'),
    meta: {
      requiresAuth: true
    }
  },
{
  path: '/impressum',
    name: 'impressum',
  component: () => import('pages/Impressum'),
  meta: {
  requiresAuth: false
}
},
{
  path: '/datenschutz',
  name: 'datenschutz',
  component: () => import('pages/Datenschutz'),
  meta: {
  requiresAuth: false
}
},
  { path: '/login', name: 'Anmelden', component: () => import('pages/Login') },
  { path: '/register', name: 'Registrieren', component: () => import('pages/Register') },
  { // Always leave this as last one
    path: '*',
    component: () => import('pages/404')
  }
]
