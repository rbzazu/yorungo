import Vue from 'vue'
import Vuex from 'vuex'

import event from './module-event'

Vue.use(Vuex)

const store = new Vuex.Store({
  state:{
    version:'0.1.25',
    levels: [
      {
        label: ' ',
        value: '0'
      },{
        label: 'Anfänger (bis 5km)',
        value: '1'
      },{
        label: 'Sportlich (bis 10km)',
        value: '2'
      },{
        label: 'Ambitioniert (mehr als 10km)',
        value: '3'
      },{
        label: 'Flexibel (mal sehen)',
        value: '4'
      }
    ]
  },
  modules: {
    event
  }
})

export default store
