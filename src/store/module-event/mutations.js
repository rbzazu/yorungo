//import Vue from "vue"

export const updateSelectedDay = (state, day) => {
  state.selectedDay = day;
}

export const updateSelectedDayDate = (state, date) => {
  state.selectedDay.date = date;
  state.selectedDay.attributes[0].customData.date = date;
}

export const updateSelectedDayZeit = (state, zeit) => {
  state.selectedDay.zeit = zeit;
  state.selectedDay.attributes[0].customData.zeit = zeit;
}

export const updateSelectedDayInfo = (state, info) => {
  state.selectedDay.attributes[0].customData.info = info;
  //Vue.set(state.selectedDay.attributes, 0, info);
}

export const updateSelectedDayOrt = (state, ort) => {
  state.selectedDay.attributes[0].customData.ort = ort;
}

export const updateEvent = (state, event) => {
  //Vue.set(state, 'event', event);
  state.event = event;
}



