// import something here
import VueFire from 'vuefire'
import firebase from 'firebase'

// Initialize Firebase
var config = {
  apiKey: "AIzaSyApOwsOCxqxmIaW21y7cJfIS9Qo7r2qmuM",
  authDomain: "lauftreff-a20e3.firebaseapp.com",
  databaseURL: "https://lauftreff-a20e3.firebaseio.com",
  projectId: "lauftreff-a20e3",
  storageBucket: "",
  messagingSenderId: "502114305370"
};

firebase.initializeApp(config);
firebase.auth().languageCode = 'de';

/*
  const messaging = firebase.messaging();
  messaging.requestPermission()
    .then(function () {
      console.log('Have permission')
      return messaging.getToken();
    })
    .then(function (currentToken) {
      if (currentToken) {
        //console.log(currentToken);
        //sendTokenToServer(currentToken);
      }
      else {
        // Show permission request.
        console.log('No Instance ID token available. Request permission to generate one.');
        // Show permission UI.
      }
    })
    .catch(function (err) {
      console.log('An error occured', err);
    })

// Callback fired if Instance ID token is updated.
  messaging.onTokenRefresh(function () {
    messaging.getToken()
      .then(function (refreshedToken) {
        console.log('Token refreshed.', refreshedToken);
        // Indicate that the new Instance ID token has not yet been sent to the
        // app server.
        //setTokenSentToServer(false);
        // Send Instance ID token to app server.
        //sendTokenToServer(refreshedToken);
        // ...
      })
      .catch(function (err) {
        console.log('Unable to retrieve refreshed token ', err);
        //showToken('Unable to retrieve refreshed token ', err);
      });
  });
*/

import { date } from 'quasar'
// leave the export, even if you don't use it
export default ({ app, router, store, Vue }) => {
  Vue.use(VueFire);

  // we add it to Vue prototype
  // so we can reference it in Vue files
  // without the need to import firebase
  Vue.prototype.$firebase = firebase

  let startday = new Date();
  //startday = date.subtractFromDate(startday, { days: 10 });
  startday = date.formatDate(startday,'YYYY-MM-DD');
  let eventsRef = firebase.database().ref('events/public').orderByChild("date").startAt(startday).limitToFirst(200);
  Vue.prototype.$eventsRef = eventsRef
  // Make sure we remove all previous listeners.
  eventsRef.off();

  //eventsRef.on('child_added', function(snapshot){ console.log(snapshot); });
}
