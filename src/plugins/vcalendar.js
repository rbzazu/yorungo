// import something here
import VCalendar from 'v-calendar';
import 'v-calendar/lib/v-calendar.min.css';

// leave the export, even if you don't use it
export default ({ app, router, Vue }) => {
  // something to do
  Vue.use(VCalendar);
}
