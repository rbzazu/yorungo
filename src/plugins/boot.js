import firebase from 'firebase'
export default ({ app, router, store, Vue }) => {
  Vue.prototype.$firebase.auth().onAuthStateChanged(function(user) {
    let usersRef = firebase.database().ref('users');
    Vue.prototype.$usersRef = usersRef;
    if ( user && user.uid ) {
      usersRef.child(user.uid).set({
        email: user.email,
        name: user.displayName ? user.displayName : user.email.replace(/@.*/, '')
      });
    }
    /* eslint-disable no-new */
    new Vue(app)
    // "app" has everything cooked in by Quasar CLI,
    // you don't need to inject it with anything at this point
  });
}
