module.exports = {
  root: true,
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module'
  },
  env: {
    browser: true
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'plugin:vue/essential',
    // https://github.com/standard/standard/blob/master/docs/RULES-en.md
    'standard'
  ],
  // required to lint *.vue files
  plugins: [
    'vue'
  ],
  globals: {
    'ga': true, // Google Analytics
    'cordova': true,
    '__statics': true
  },
  // add your custom rules here
  'rules': {
    // allow async-await
    'generator-star-spacing': 'off',

    // allow paren-less arrow functions
    'arrow-parens': 0,
    'one-var': 0,

    'import/first': 0,
    'import/named': 2,
    'import/namespace': 2,
    'import/default': 2,
    'import/export': 2,
    'import/extensions': 0,
    'import/no-unresolved': 0,
    'import/no-extraneous-dependencies': 0,

    'semi': 0,
    'indent': 0,
    'comma-spacing':0,
    'space-before-function-paren':0,
    'key-spacing':0,
    'space-in-parens':0,
    'handle-callback-err':0,
    'curly':0,
    'no-template-curly-in-string':0,
    'space-before-blocks':0,
    'quotes':0,
    'spaced-comment':0,
    'comma-style':0,
    'arrow-spacing':0,
    'func-call-spacing':0,
    'space-infix-ops':0,
    'block-spacing':0,
    'no-multiple-empty-lines':0,
    'keyword-spacing':0,
    'eqeqeq':0,
    'new-cap':0,
    'comma-dangle':0,

    'brace-style': [2, 'stroustrup', { 'allowSingleLine': true }],

    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0
  }
}
