// Import and configure the Firebase SDK
// These scripts are made available when the app is served or deployed on Firebase Hosting
// If you do not serve/host your project using Firebase Hosting see https://firebase.google.com/docs/web/setup
importScripts('https://www.gstatic.com/firebasejs/4.10.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.10.1/firebase-messaging.js');

// Initialize Firebase
var config = {
  apiKey: "AIzaSyApOwsOCxqxmIaW21y7cJfIS9Qo7r2qmuM",
  authDomain: "lauftreff-a20e3.firebaseapp.com",
  databaseURL: "https://lauftreff-a20e3.firebaseio.com",
  projectId: "lauftreff-a20e3",
  storageBucket: "",
  messagingSenderId: "502114305370"
};

firebase.initializeApp(config);
var messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
  var notificationTitle = payload.notification.title;
  var notificationOptions = {
    body: payload.notification.message,
    icon: '/statics/quasar-logo.png'
  };

  return self.registration.showNotification(notificationTitle,notificationOptions);
});
