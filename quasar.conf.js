// Configuration for your app
const CopyWebpackPI = require('copy-webpack-plugin');

module.exports = function (ctx) {

  return {
    plugins: [
      'boot','axios', 'firebase'
    ],
    css: [
      'app.styl'
    ],
    extras: [
      ctx.theme.mat ? 'roboto-font' : null,
      'material-icons'
      // 'ionicons',
      // 'mdi',
      // 'fontawesome'
    ],
    supportIE: false,
    build: {
      scopeHoisting: true,
      vueRouterMode: 'history',
      // gzip: true,
      // analyze: true,
      // extractCSS: false,
      // useNotifier: false,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules|quasar)/
        });
        cfg.plugins.push(
          new CopyWebpackPI([
            {
              from: 'root/'
            }
          ])
        );
      }
    },
    devServer: {
      // https: true,
      // port: 8080,
      open: true // opens browser window automatically
    },
    // framework: 'all' --- includes everything; for dev only!
    framework: {
      i18n: 'de',
      components: [
        'QLayout',
        'QLayoutHeader',
        'QLayoutFooter',
        'QLayoutDrawer',
        'QPageContainer',
        'QPage',
        'QToolbar',
        'QToolbarTitle',
        'QBtn',
        'QBtnGroup',
        'QIcon',
        'QList',
        'QListHeader',
        'QItem',
        'QItemMain',
        'QItemSide',
        'QDatetime',
        'QModal',
        'QModalLayout',
        'QScrollArea',
        'QCheckbox',
        'QChip',
        'QSelect',
        'QItemTile',
        'QItemSeparator',
        'QSlider',
        'QPageSticky',
        'QInput',
        'QTabs',
        'QRouteTab',
        'QCollapsible',
        'QCard',
        'QCardMain',
        'QCardActions',
        'QCardSeparator'
      ],
      directives: [
        'Ripple','TouchHold'
      ],
      plugins: [
        'Notify','Loading','Platform','ActionSheet','Dialog'
      ]
    },
    // animations: 'all' --- includes all animations
    animations: [
    ],
    pwa: {
      manifest: {
        name: 'Yorungo',
        short_name: 'Yorungo',
        description: 'Sport in selbstorganisierten Gruppen',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },
    cordova: {
      // id: 'org.cordova.quasar.app'
    },
    electron: {
      extendWebpack (cfg) {
        // do something with cfg
      },
      packager: {
        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Window only
        // win32metadata: { ... }
      }
    },

    // leave this here for Quasar CLI
    starterKit: '1.0.0'
  }
}
